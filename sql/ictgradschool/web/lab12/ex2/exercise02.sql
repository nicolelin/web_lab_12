-- Answers to Exercise 2 here

-- CREATE TABLE
DROP TABLE IF EXISTS web_lab12_ex02;

CREATE TABLE IF NOT EXISTS web_lab12_ex02 (
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(16),
  first_name VARCHAR(16),
  last_name VARCHAR(16),
  email TEXT,
  PRIMARY KEY (id)
);

-- INSERT VALUES INTO TABLE
INSERT INTO web_lab12_ex02 (username, first_name, last_name, email) VALUES
  ('programmer1','Bill','Gates','bill@microsoft.com'),
  ('programmer2','Peter','Parker','spiderman@marvel.com'),
  ('programmer3','Pete','Blake','pb@teamnz.com'),
  ('llawless','Lucy','Lawless','lucy@lawless.co.nz'),
  ('elon','Elon','Musk','elon@spacex.com'),
  ('trump','Donald','Trump','thedon@trump.com'),
  ('tcook','Tim','Cook','tim@apple.com'),
  ('zuck','Mark','Zuckerberg','mark@facebook.com'),
  ('snadella','Satya','Nadella','s.nadella@microsoft.com'),
  ('winston','Winston','Peters','winston@nzfirst.co.nz');