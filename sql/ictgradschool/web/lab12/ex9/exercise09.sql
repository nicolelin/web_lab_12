-- Answers to Exercise 9 here

-- Devise SQL statements that display the following:

# ● All information about all the members of the video store
SELECT * FROM web_lab12_ex03;

# ● Everything about all the members of the video store, except the number of video # hires they have
SELECT customer_id,name,gender,year_born,year_joined FROM web_lab12_ex03;

# ● All the titles to the articles that have been written
SELECT article_title FROM web_lab12_ex04;

# ● All the directors of the movies the video store has (without repeating any names).
SELECT DISTINCT movie_director FROM web_lab12_ex06;

# ● All the video titles that rent for $2 or less a week.
SELECT movie_title, movie_weekly_rate FROM web_lab12_ex06 WHERE movie_weekly_rate <= 2;

# ● A sorted list of all the usernames that have been registered.
SELECT username FROM web_lab12_ex02 ORDER BY username ASC;

# ● All the usernames where the user’s first name starts with the letters ‘Pete’.
SELECT username FROM web_lab12_ex02 WHERE first_name LIKE 'Pete%';

# ● All the usernames where the user’s first name or last name starts with the letters ‘Pete’.
SELECT username FROM web_lab12_ex02 WHERE first_name LIKE 'Pete%' OR last_name LIKE 'Pete%';