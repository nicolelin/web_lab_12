-- Answers to Exercise 8 here

-- INSTRUCTIONS
# Modify tables from prior #

# ● Delete a row from one table.
# ● Delete a column from the same table.
# ● Now delete the entire table (and then reinstate it with the original exerciseXX.sql
# file you have).
# ● Change a value that is a string
# ● Change a value that is a numeric value
# ● Change a value that is a primary key.

# Note : since entries in the members table are referenced by the movies table, you can't delete the member table without deleting Movies first.

-- DATA MANIPULATIONS

-- MODIFYING ex 06 table (movies)

# ADD ROW
INSERT INTO web_lab12_ex06 (movie_id, movie_title, movie_director, movie_weekly_rate, movie_loaned_to) VALUES
  (7382, 'Toy Story 3', 'Lee Unkrich', 4, 10),
  (8827, 'The Incredibles', 'Brad Bird', 2, NULL),
  (9210, 'Shrek', 'Andrew Adamson', 2, 2),
  (9892, 'Trolls', 'Mike Mitchell', 6, 11),
  (9895, 'Kung Fu Panda', 'Mark Osborne', 4, 6);

# DELETE ROW
DELETE FROM web_lab12_ex06
WHERE movie_id = 9892;

# ADD COLUMN
ALTER TABLE web_lab12_ex06
  ADD movie_studio VARCHAR(32),
  ADD movie_genre VARCHAR(16),
  ADD movie_rating INT(5);

UPDATE web_lab12_ex06
SET movie_studio = 'Pixar'
WHERE movie_id IN (1243, 2341, 3118, 3463, 4117, 5279, 6804, 7382, 8827);

UPDATE web_lab12_ex06
SET movie_studio = 'Dreamworks Animation'
WHERE movie_id IN (9210, 9895);

UPDATE web_lab12_ex06
SET movie_genre = 'Animation'
WHERE movie_studio IN ('Pixar', 'Dreamworks Animation');

# DELETE COLUMN
ALTER TABLE web_lab12_ex06
  DROP COLUMN movie_rating;

# DELETE TABLE (uncomment to run)
-- DROP TABLE web_lab12_ex06;

# ADD TABLE BACK (RUN exercise06.sql AGAIN)

# CHANGE VALUE (STRING)

UPDATE web_lab12_ex06
SET movie_studio = 'Pixar Animation Studios' WHERE movie_studio = 'Pixar';

UPDATE web_lab12_ex06
SET movie_studio = REPLACE(movie_studio,'Pixar Animation Studios','Pixar');

# CHANGE VALUE (INT)
-- Add back rating column
ALTER TABLE web_lab12_ex06
  ADD movie_rating INT(10);

ALTER TABLE web_lab12_ex06
  MODIFY COLUMN movie_rating INT(1);

UPDATE web_lab12_ex06
SET movie_rating = 5
WHERE movie_rating IS NULL AND movie_studio = 'Pixar';

# CHANGE VALUE (PRIMARY KEY)
UPDATE web_lab12_ex06
    SET movie_id = 8888 WHERE movie_id = 8827;

SELECT *
FROM web_lab12_ex06;