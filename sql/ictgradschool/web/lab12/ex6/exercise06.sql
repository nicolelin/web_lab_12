-- Answers to Exercise 6 here

-- INSTRUCTIONS

/* Extend the idea of the video store from Exercise 3 so there is a table that represents the movies the rental store has.

In addition to a unique id for the movie (bar-code?) it should store:

 * the title of the movie,
 * who directed it, and its weekly charge-out rate.

 Additionally, include a column (foreign key) that links the movie to who in the members table has the video out on loan.

In setting up entries for this table, be sure to include some movie examples by the same director (i.e. so there are two or more rows in the table where the director’s name is the same). This will be used in a later exercise.

In terms of charge-out rate, use a mix of the values $2, $4, and $6 when initializing the table.

Note : You may find that the sequence of table creation in your Exercise 6 matters.
Since the Movies table references entries in your members table, those entries must exist in the members table before the entries in the Movies table that refer to them can be created. */

-- CREATE TABLE
DROP TABLE IF EXISTS web_lab12_ex06;

CREATE TABLE IF NOT EXISTS web_lab12_ex06 (
  movie_id INT NOT NULL,
  movie_title VARCHAR(32) NOT NULL,
  movie_director VARCHAR(32) NOT NULL,
  movie_weekly_rate INT,
  movie_loaned_to INT,
  PRIMARY KEY (movie_id),
  FOREIGN KEY (movie_loaned_to) REFERENCES web_lab12_ex03(customer_id)
);

-- INSERT VALUES INTO TABLE
INSERT INTO web_lab12_ex06 (movie_id, movie_title, movie_director, movie_weekly_rate, movie_loaned_to) VALUES
  (1243,'Toy Story','John Lasseter',2,25),
  (2341,'Finding Nemo','Andrew Stanton',2,12),
  (3118,'Ratatouille','Brad Bird',2,18),
  (3463,'WALL-E','Andrew Stanton',4,1),
  (4117,'Up','Pete Docter',4,7),
  (5279,'The Good Dinosaur','Peter Sohn',6,25),
  (6804,'Finding Dory','Andrew Stanton',6,22);