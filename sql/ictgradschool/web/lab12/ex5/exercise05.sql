-- Answers to Exercise 5 here
-- (extends code from exercise 2)

-- CREATE TABLE
DROP TABLE IF EXISTS web_lab12_ex05;

CREATE TABLE IF NOT EXISTS web_lab12_ex05 (
  id INT NOT NULL,
  username VARCHAR(16),
  first_name TEXT,
  last_name TEXT,
  email TEXT,
  PRIMARY KEY (username)
);

-- INSERT VALUES INTO TABLE

# When username is the primary key AND is duplicated in different rows:
INSERT INTO web_lab12_ex05 (id, username, first_name, last_name, email) VALUES
  (1,'programmer1','Bill','Gates','bill@microsoft.com'),
  (2,'programmer2','Peter','Parker','spiderman@marvel.com'),
  (3,'programmer3','Pete','Blake','pb@teamnz.com'),
  (3,'programmer3','Lucy','Lawless','lucy@lawless.co.nz');

# Result:
# [2017-05-03 14:14:37] [23000][1062] Duplicate entry 'programmer3' for key 'PRIMARY'