-- Answers to Exercise 7 here

-- INSTRUCTIONS

/* Continue the development of articles database example from Exercise 4.

Add a table for storing comments that are posted about a particular article (imagine them being displayed at the bottom of the article when that particular page is called up in the web site).

Comments should have their own unique ID as its primary key. It should also contain a foreign key of the article ID the comment applies to.

The comment ID should be automatically generated - Look into the AUTO_INCREMENT qualifier. */

-- CREATE TABLE
DROP TABLE IF EXISTS web_lab12_ex07;

CREATE TABLE IF NOT EXISTS web_lab12_ex07 (
  comment_id INT NOT NULL AUTO_INCREMENT,
  article_id INT NOT NULL,
  comment_content TEXT,
  PRIMARY KEY (comment_id),
  FOREIGN KEY (article_id) REFERENCES web_lab12_ex04(article_id)
);

-- INSERT VALUES INTO TABLE
INSERT INTO web_lab12_ex07 (article_id, comment_content) VALUES
  (1,'Very interesting!'),
  (2,'Delightful analysis.'),
  (3,'I think I''m crying. It''s that revolutionary.'),
  (1,'Mission accomplished. It''s elegant.'),
  (1,'Immensely thought out! I''m in!'),
  (2,'It''s incredible not just sublime!'),
  (1,'Graceful work you have here.'),
  (3,'Revolutionary. This will change the game for sure!');