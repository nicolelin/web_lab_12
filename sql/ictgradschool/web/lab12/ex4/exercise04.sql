-- Answers to Exercise 4 here

# Instructions
/* Taking as inspiration the idea of a web site that displays articles, develop an SQL table suitable for storing such information.

Give each article in the table:
 * an integer id for uniquely identifying it,
 * along with a table column for storing the title of the article
 * along with (of course) a table column for the text of the article.

As a source of sample text for the articles, look to a lorem ipsum generator. Only use a paragraph or two. */

-- CREATE TABLE
DROP TABLE IF EXISTS web_lab12_ex04;

CREATE TABLE IF NOT EXISTS web_lab12_ex04 (
  article_id INT NOT NULL AUTO_INCREMENT,
  article_title TEXT,
  article_content TEXT,
  PRIMARY KEY (article_id)
);

-- INSERT VALUES INTO TABLE
INSERT INTO web_lab12_ex04 (article_title, article_content) VALUES

  -- article 1 --

  ('Python vs. Ruby: Which Is Best for Web Development?','Python and Ruby are among some of the most popular programming languages for developing websites, web-based apps, and web services.

In many ways, the two languages have a lot in common. Visually they are quite similar, and both provide programmers with high-level, object-oriented coding, an interactive shell, standard libraries, and persistence support. However, Python and Ruby are worlds apart in their approach to solving problems because their syntax and philosophies vary greatly, primarily because of their respective histories.

Which one to implement for web development requires some thought because all languages have strengths and weaknesses and your decision will have consequences.

The Basics
Python was developed organically in the scientific space as a prototyping language that easily could be translated into C++ if a prototype worked. This happened long before it was first used for web development. Ruby, on the other hand, became a major player specifically because of web development; the Rails framework extended Ruby''s popularity with people developing complex websites.

Which programming language best suits your needs? Here is a quick overview of each language to help you choose.

Approach: One Best Way Vs. Human-Language Python
Python takes a direct approach to programming. Its main goal is to make everything obvious to the programmer. In Python, there is only one "best" way to do something. This philosophy has led to the language being very strict in its layout.

Python''s core philosophy consists of three key hierarchical principles:

Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
This regimented philosophy results in Python being eminently readable and easy to learn—and why Python is great for beginning coders. Python has a big foothold in introductory programming courses. Its syntax is very simple, with little to remember. Because its code structure is explicit, the developer can easily tell where everything comes from, making it relatively easy to debug.

Python''s hierarchy of principles is evident in many aspects of the language. Its use of whitespace to do flow control as a core part of the language syntax differs from most other languages, including Ruby. The way you indent code determines the meaning of its action. This use of whitespace is a prime example of Python''s "explicit" philosophy, the shape of a Python app spells out its logic and how the app will act.

Ruby
In contrast to Python, Ruby focuses on "human-language" programming, and its code reads like a verbal language rather than a machine-based one, which many programmers, both beginners and experts, like. Ruby follows the principle of "least astonishment," and offers myriad ways to do the same thing. These similar methods can have multiple names, which many developers find confusing and frustrating.

Unlike Python, Ruby makes use of "blocks," a first-class object that is treated as a unit within a program. In fact, Ruby takes the concept of OOP (Object-Oriented Programming) to its limit. Everything is an object—even global variables are actually represented within the ObjectSpace object. Classes and modules are themselves objects, and functions and operators are methods of objects. This ability makes Ruby especially powerful, especially when combined with its other primary strength: functional programming and the use of lambdas.

In addition to blocks and functional programming, Ruby provides programmers with many other features, including fragmentation, hashable and unhashable types, and mutable strings.

Ruby''s fans find its elegance to be one of its top selling points. At the same time, Ruby''s "magical" features and flexibility can make it very hard to track down bugs.

Communities: Stability vs. Innovation
Although features and coding philosophy are the primary drivers for choosing a given language, the strength of a developer community also plays an important role. Fortunately, both Python and Ruby boast strong communities.

Python
Python''s community already includes a large Linux and academic community and therefore offers many academic use cases in both math and science. That support gives the community a stability and diversity that only grows as Python increasingly is used for web development.

Ruby
However, Ruby''s community has focused primarily on web development from the get-go. It tends to innovate more quickly than the Python community, but this innovation also causes more things to break. In addition, while it has gotten more diverse, it has yet to reach the level of diversity that Python has.

Final Thoughts
For web development, Ruby has Rails and Python has Django. Both are powerful frameworks, so when it comes to web development, you can''t go wrong with either language. Your decision will ultimately come down to your level of experience and your philosophical preferences.

If you plan to focus on building web applications, Ruby is popular and flexible. There is a very strong community built upon it and they are always on the cutting edge of development.

If you are interested in building web applications and would like to learn a language that''s used more generally, try Python. You''ll get a diverse community and lots of influence and support from the various industries in which it is used.'),

  -- article 2 --

  ('Design Thinking Needs To Think Bigger
','The following is the second of two excerpts from The Way to Design, a guide to becoming a designer founder and to building design-centric businesses. It was adapted and reprinted with the author’s permission. Read the first one, on the case against empathy, here.

Design thinking is one of the most important ideas of the 21st century. The methodology’s impact on product design, how organizations go about solving problems, and how we live our everyday lives has been profound. And its influence has expanded far beyond business and design circles. Universities, nonprofits, even science labs run design sprints based on design-thinking principles. The most popular course at Stanford is one on how to approach your life as a design challenge. The concept is even taught at some elementary schools.

But it’s been 15 years—a generation—since David Kelley had his epiphany to stop calling Ideo’s approach “design” and start branding it as “design thinking.” And much has changed in that time.

Each day we now generate 2.5 quintillion bytes of data—from internet posts, mobile phone activity, internet-of-things sensors, purchase transactions, and more. So much data that over 90% of it in existence was created in just the last two years. Two years of Twitter tweets produce more words than are contained in all the books ever printed, combined. At about the same time that Kelley’s “design thinking” lightbulb was going off, in 2002 a full human genome sequence cost $100 million. These days it can be done for $1,000. And by 2020 it’ll cost less than a movie ticket.

Not only is the world more infinitely complex than at the turn of the century, it is also profoundly more intertwined. We are halfway to connecting everyone on the planet, with 3.7 billion internet users worldwide. In the U.S., 99% of 18- to 29-year-olds use the internet. Smartphones have become ubiquitous: roughly half the world’s adult population owns one, and it’s projected that by 2020 the figure will climb to 80%. WhatsApp was founded less than a decade ago, but now traffics in 10 billion more messages a day than the SMS global text-messaging system.

We live in a massively complex, intricately interconnected global system. And it’s increasingly impossible to be designers (or human beings) without taking into account how we affect and are, in turn, affected by all the moving pieces of this organic machine. “The more complex an organism is,” says artist and teacher Adam Wolpert, “the more capable it becomes. And the more capable it is, the more it can address challenges and seize opportunities. The downside of that is, the more complex it becomes, the more vulnerable it becomes.” The challenge for designers is learning how to balance the production of evermore complex capability against the threat of a resultant breakdown. That’s why I think design thinking, which emphasizes solving problems holistically, needs to look at a bigger whole by incorporating another body of thought: systems thinking.

What is systems thinking?
Systems thinking isn’t new—though it may be unfamiliar to many designers. It’s a mode of analysis that’s been around for decades. But it has newfound relevance for today’s everything-is-networked, Big Data world. Systems thinking is a mind-set—a way of seeing and talking about reality that recognizes the interrelatedness of things. System thinking sees collections of interdependent components as a set of relationships and consequences that are at least as important as the individual components themselves. It emphasizes the emergent properties of the whole that neither arise directly, nor are predictable, from the properties of the parts.

Systems thinking can be used to explain and understand everything from inventory changes in a supply chain, to populations of bacteria and their hosts, to the instability in Syria, to the seemingly irrational behavior of certain elected officials. The vocabulary of formal systems thinking is one of causal loops, unintended consequences, emergence, and system dynamics. Practicing systems theorists employ tools such as systemigrams, archetypes, stock and flow diagrams, interpretive structural modeling, and systemic root cause analysis—all of which is beyond the scope of this post. For the purposes of this discussion, I’ll simply introduce the Iceberg Model and briefly discuss two key concepts in systems thinking—emergence and leverage points.

The Iceberg Model is a helpful way to explain the concerns that drive systems thinking. Events are at the top of the iceberg. They’re incidents that we encounter from day to day—the hurly-burly of life. Patterns are the accumulated habits or behavioral “memories” that result from repeated, unconsidered reaction to events. Systemic structures are how the components of the system are organized. These structures generate the patterns and events that confront us. Mental models are the assumptions we have about how the world works; they give birth to systemic structures. Values are the vision we have for our future—what we aspire to. They’re the basis for our mental models.

Mostly we live at the level of events, because it’s easier to notice events than it is to discern hidden patterns and systemic structures. Even though it’s underlying systems that are actually driving the events we’re captive to. It’s there, at the tip of the iceberg, that we expend most of our energies and attention, and like the Titanic, it’s there that we run aground because we don’t see the truth of the problem, the variables and influences lying below the surface. We take actions without understanding the impact of those actions on the system, making the situation worse.

As an apocryphal illustration, let’s say, your favorite fancy coffeehouse (or Philz, for those of us in the Bay Area) serves you an anomalously bad cup of joe. That would be an event. A pattern would be noticing there’s a higher frequency of bad coffees produced during shift changes from morning to afternoon to evening barista staff. Perhaps the systemic structure generating this pattern of unpalatable coffees is that the shift changes are scheduled so as there’s no overlap between the incoming and outgoing teams of baristas.

The mental model that the baristas hold leads them to believe that they’re only responsible for the mochas and lattes that they make, not the team after them. And say, the value that drives that belief is one of competition—of wanting to make better coffees than the other shifts, and therefore not being concerned about the pour-over apparatus being properly cleaned, or the beans correctly ground and apportioned, at the end of a shift.

It’s usually the case that moral character or human error are blamed for what are really system failures. The people who made the mistakes—the “bad apples”—need to be reprimanded, retrained, or fired. Out with the offending baristas! But systems thinkers understand that these are symptoms and not causes. Systems-savvy designers will know the real answer is to unearth what patterns or assumptions are generating those suboptimal behaviors—the bad containers, as Stanford psychologist Philip Zimbardo puts it, rather than bad apples. Not just what happened and when, but how and why these things happened. In the apocryphal example, maybe the solution is to stagger the staff turnovers, so that baristas from earlier and later shifts are always intermingled.

How to understand systems thinking
There are two key concepts to understanding systems thinking. The first is emergence. What makes a system a system rather than just a collection of parts is that the components are interconnected and interdependent. Their interconnectedness creates feedback loops, which change the behavior of the system—in fact, they define the behavior of the system. Emergent properties arise that exist only in the system as a totality, and not in its disparate components, making it impossible to understand the system without looking at the whole.

You can’t understand how we get to an anthill by looking at a single antenna or thorax. A Tesla driving down Highway 280 is an emergent property of the innumerable parts that go into making the car—as well as the national grid of recharging stations that had to be built and the web of regulatory oversight that needed to be navigated. In the inextricably connected world we live in, it’s no longer possible or wise to solve for the part without due consideration of the sum of the parts.

So how are designers supposed to address this onslaught of socioeconomic, techno-political complexity? I think the trick is to analyze systems with an eye toward finding leverage points—the second key concept in systems thinking. Rather than attempt to design a wholly new, perfect solution, oftentimes it’s better to find areas where an incremental change will lead to significant renovation in the system. The smallest nudge for the biggest effect.

“Everything is networked now,” says Pinterest cofounder Evan Sharp, whom I interviewed for my project on designer founders. “All of culture, all of communications, it all is going through networks.” Therefore, at the scale of 7 billion people, “any small, little improvement you make has massive aggregate value.” This will cut against the grain of most designers’ instincts, because the end result will likely be far from an ideal proposed design, but designing for the real world means dealing with the practical constraints of that reality and trying to make refinements in the face of compromise.

Now, I don’t want to oversell systems thinking. It’s not always possible in real-world cases to reasonably model very complex systems in ways that lead to good design strategies and outcomes. Systems thinking will also be novel and perhaps somewhat jarring to many designers, because as designers we’re usually laser-focused on a single, discrete design problem. But when appropriate, applying a systems mind-set to design thinking will give designer founders a powerful tool for circumnavigating the problems of the age. Focus on relationships over parts; recognize that systems exhibit self-organization and emergent behaviors; analyze the dynamic nature of systems in order to understand and influence the complex societal, technological, and economic ecosystem in which you and your organization operate.

The challenge is to rise above the distraction of the details and widen your field of vision. Try to see the whole world at once and make sense of it. It’s a heady challenge, but you either design the system or you get designed by the system.

Steve Vassallo is general partner at Foundation Capital and the author of The Way to Design.
'),

  -- article 3 --

  ('Chinese internet giant Tencent opens artificial intelligence lab in Seattle','Chinese tech titan Tencent has announced that it’s opening a new artificial intelligence (AI) lab in Seattle, with speech recognition expert Dr. Yu Dong, formerly a principal researcher at Microsoft’s Speech and Dialog Group, leading the initiative.

The new lab, which was first rumored last month, will focus on both “fundamental research and practical application of artificial intelligence,” according to a statement issued by the company, and will push to develop AI’s “understanding, decision-making and creativity,” while supporting Tencent’s AI efforts across its range of businesses, which include gaming and social media.

Tencent first launched an AI lab in Shenzhen, China last April, with a focus on machine learning, computer vision, speech recognition, and natural language processing (NLP). According to the company, a number of its products already use technology created in the lab — including its wildly popular WeChat messaging app. The Seattle AI hub will focus more on speech recognition and NLP.

“We hope that AI Lab will become more than a laboratory, but a connector,” explained Tencent’s Dr. Zhang Tong, who will serve as the lab’s executive director. “By bringing together the world’s leading experts in the field, we hope to further drive fundamental research on AI to expand its influence and enhance its practicality.”

The decision to open a hub in Seattle positions Tencent to hire top engineers and AI professionals in one of the country’s top tech hubs outside of Silicon Valley. Indeed, it’s a move echoed by other Chinese tech juggernauts, such as Alibaba, which has had a secretive base in Seattle since 2014, though it recently upped sticks and moved to nearby Bellevue. Elsewhere, Baidu, the “Google of China,” opened a new Silicon Valley arm last year dedicated to self-driving cars, while ride-hailing giant Didi Chuxing opened a new AI hub in Mountain View back in March.

“With the establishment of the AI Lab in Seattle, I believe that there will be more top talent joining AI Lab to help further promote AI development around the world,” added Dr. Yu.');